#pragma once
#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

class Story
{
private:
	string s[10];		//ストーリーを保存する変数

public:
	Story();			//デフォルトコンストラクタ
	~Story();			//デコンストラクタ

	void Draw();		//情報を表示する関数
};