﻿#include "Story.h"

//メイン文
int main()
{
	int num = 10;			//人数
	Story *story;			//ストーリーオブジェクト
	story = new Story();	//デフォルトコンストラクタで実装


	//ストーリーを表示する関数を呼び出し
	story->Draw();


	delete story;			//削除して♥

	return 0;
}